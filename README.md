# single-spa-from-scratch-workshop
Using single spa without the helpers workshop.

## Goal
A lot of single spa tutorials are top down, showing full applications.
That's extremely useful to show the power of single-spa as a tool, but requires you to learn
the api before moving on to what single-spa is doing. The goal with this workshop is to strip
the classic example to a minimum point and build up from there, hopefully increasing total
understanding of single-spa inside and out.

Moving from an index html to a working single spa example without react, angular, or any helpers

## running the workshop
1. Have node/npm installed and run `npx http-server`
2. Open browser to http://localhost:8080

## Abstract steps
1. Create two single spa apps that are simple divs with text.
-- fun challenge don't use the route in the activity function.
2. Convert one of the two apps to be react (WITHOUT single-spa-react)
3. Create a parcel in app1 (WITHOUT single-spa-react)
4. Use the parcel in app2
